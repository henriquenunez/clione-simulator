# clione-simulator

## Requirements:

- three.js
- chart.js
- lodash.js
- bootstrap.css

## To run:

Gather all the mentioned files and put them in the /js folder. (I am not redistributing them because they are already available from the CDNs)

Then:

`python -m http.server`

In the project root folder.

