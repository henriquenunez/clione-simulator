// TODO: find a better name for this

const average = arr => arr.reduce((a,b) => a + b, 0) / arr.length;

function createVoxelsFromCSV(parsed, opts = { // Should contain the col mapping, extents, and other things maybe
    'Xres' : 1.0,
    'Yres' : 1.0,
    'Zres' : 1.0,
}) {
    let structure = {};

    parsed.forEach(element => {
        // Compute where it should lie.

        let x = parseInt(element['X'] / opts['Xres']);
        let y = parseInt(element['Y'] / opts['Yres']);
        let z = parseInt(element['Z'] / opts['Zres']);

        let refString = x.toString() + ';' + y.toString() + ';' + z.toString();

        if (structure[refString] === undefined) { // Create key for the voxel
            structure[refString] = [];
        }

        structure[refString].push(element['H']);
    });

    // Now average each voxel
    for (let voxelIndexer in structure) {
        structure[voxelIndexer] = average(structure[voxelIndexer]);
    }

    console.log(JSON.stringify(structure));

    return structure;
}

async function importFile(file, callback) {
    await Papa.parse(file, { 'header' : true, 'complete' : (res, file) => {
        const parsed = res.data; // Todo error checking
        let structure = createVoxelsFromCSV(parsed);
        console.log('Actually finished importing file');

        callback(structure);
    }});
}

function posStringToPos(posStr) {
    let spl = posStr.split(';');

    return { 'x': spl[0], 'y': spl[1], 'z': spl[2] }
}

export { importFile, posStringToPos }