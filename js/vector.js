
function vec2(x = 0, y = 0) {
    return { x : x, y : y };
}

function pol2rec(rho, theta) {
    let x = Math.cos(theta) * rho;
    let y = Math.sin(theta) * rho;

    return vec2(x, y);
}

function vec2Add(vecA, vecB) {
    return vec2(vecA.x + vecB.x, vecA.y + vecB.y);
}
