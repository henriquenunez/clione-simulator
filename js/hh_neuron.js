
// Constants
var C_m = 1       // microfarads over cm²
var V_Na = 50     // milivolts
var V_K = -77     // milivolts
var V_L = -54.387 // milivolts
var g_Na = 120    // milisiemens over cm²
var g_K = 36      // milisiemens over cm²
var g_L = 0.3     // milisiemens over cm²
var area = 7.854E-3 // cm²

// Initial conditions
var V_o = -65
var m_o = 0.053
var h_o = 0.6
var n_o = 0.318

var e = Math.exp

// Functions for everything
var alpha_m =  (V) =>  { return  0.1 * (-V - 40) / (e((-V - 40)/10)-1) }
var beta_m =  (V) =>  { return  4 * e((-V - 65)/18) }

var alpha_h =  (V) =>  { return  0.07 * e((-V - 65)/20) }
var beta_h =  (V) =>  { return  1 / (e((-V - 35)/10)+1) }

var alpha_n =  (V) =>  { return  0.01 * (-V - 55) / (e((-V - 55)/10)-1) }
var beta_n =  (V) =>  { return  0.125 * e((-V - 65)/80) }

// First order coupled differential equations
var dm =  (V, m) => { return alpha_m(V) * (1-m) - beta_m(V) * m }
var dh =  (V, h) => { return alpha_h(V) * (1-h) - beta_h(V) * h }
var dn =  (V, n) => { return alpha_n(V) * (1-n) - beta_n(V) * n }

var dV = (V, m, h, n, I) => { return I - g_L * (V - V_L) - g_Na * m**3 * h * (V - V_Na) - g_K * n**4 * (V - V_K) }

var iterEpsilon = 0.01;

class Neuron {
    constructor () {
        this.V = V_o;
        this.m = m_o;
        this.h = h_o;
        this.n = n_o;
        this.I = 0;
        this.i = 0;
        this.V_s = [];
        this.t_s = [];
    }

    // Iterating with the Euler's method
    doIteration () {
        //I_s.append(I)
        this.V_s.push(this.V)
        this.t_s.push(this.i)
        
        let new_V = this.V + iterEpsilon * dV(this.V, this.m, this.h, this.n, this.I) / C_m
        

        this.m = this.m + iterEpsilon * dm(this.V, this.m)
        this.h = this.h + iterEpsilon * dh(this.V, this.h)
        this.n = this.n + iterEpsilon * dn(this.V, this.n)
        this.V = new_V

        this.i += 1
    }

    iterUntil (until) {
        const selfObj = this;

        for (var t = 0.0; t < until; t += 1) {
            selfObj.doIteration()
        }
    }
}

export { Neuron }
