// Model Parameters
const e = 3
const mu = 0.0021
const S = 4
const g = 0.112; // TODO change

var iterEpsilon = 0.005;

// function I_syn(t0, t, t1) {
//     g_syn * r(t) * ()
// }

// function configSyn(config) {
//     if (config['type'] == 'exc') {
        
//     } else if (config['type'] == 'inh') {

//     }

//     let r = (t0, t, t1) => {
//         if (t < t1) {
//             return 
//         }
//     }

//     let I_syn = (t0, t, t1) => {

//     }
// }

class Neuron {
    constructor (_name) {
        
        this.name = _name;
        this.I = 0;
        
        this.x = -1.5;
        this.y = (1-5*(1.5**2))/2;
        this.z = (0.0021*4*0.1)/1.0021;
        this.t = 0;
        this.i = 0;
        
        this.x_s = [];
        this.y_s = [];
        this.z_s = [];
        this.t_s = [];

        this.activeSynapses = []; // Incoming active synapses
        this.wiredNeurons = [];

        this.synCurr = 0;
        this.pulseTimeout = 100;
    }

    /**
     * Checks the neighboring neurons and performs the synaptic balance.
     * >>> Actually run the synapses 
     */
    interact () {
        // Excitatory and Inhibitory Synapses are modelled by their parameters
        // this.activeSynapses.forEach(s => {
        //     this.synapticCurrent += (s.neuron.x - this.x) * s.g; // Ohm's law
        // });

        // Send signals
        if (this.x > 1) {
            this.pulseTimeout--;
            if (this.pulseTimeout == 0) {
                this.synapticPulse();
                this.pulseTimeout = 100;
            }
        }

        // Receive signals
        this.synCurr = 0;

        if (this.activeSynapses.length == 0) {
            return;
        }

        // console.log(this.activeSynapses.length);

        let synNext = this.activeSynapses.map((n) => {return n.next()});
        let synDone = synNext.map((s) => { if (s.done == true) return 1; else return 0;});
        this.synCurr = synNext.map((s) => { return s.done ? 0 : s.value }).reduce((a, b) => a + b);
        //console.log(this.synCurr);
        
        // Deleting the elements where the index is 1 (finished)
        this.activeSynapses = this.activeSynapses.filter((el, idx) => {
            if (synDone[idx] == 1) // Remove if done
                return false;
            else return true;
        });
    }

    // Iterating with the Euler's method
    doIteration () {

        // Only record the 10000 first iterations
        if (this.i < 10000000) {
            this.x_s.push(this.x);
            this.y_s.push(this.y);
            this.z_s.push(this.z);
            this.t_s.push(this.t);
        }
      
        let noise = (Math.random() - 0.5) * 2;

        let mds = this.x + iterEpsilon * (this.y + 3 * (this.x * this.x) - (this.x * this.x * this.x) - this.z + e + this.synCurr + noise);; 
        this.x = mds;
        this.y = this.y + iterEpsilon * (1 - 5 * (this.x * this.x) - this.y);
        this.z = this.z + iterEpsilon * mu * ((-this.z) + S * (this.x+1.6));
        this.t = this.t + iterEpsilon;
      
        this.i += 1;

        //console.log("Iteration " + this.name);
    }

    /**
     * Creates a connection from this neuron to otherneuron.
     * @param {Neuron} receivingNeuron
     * @param {Object} params Must contain the 'gMax', 'tau' and 'ESyn' fields, that model the function
     */
    addSynapse(receivingNeuron, params) {
        this.wiredNeurons.push({ 'n' : receivingNeuron, 'p' : params });
    }

    /**
     * This function is called whenever the action potential is generated, and should
     * send the information to the other neurons.
     */
    synapticPulse() {
        //console.log("Synapticpulse");

        this.wiredNeurons.forEach((s) => {
            
            function* syn() {
                const neuron = s.n;
                const param = s.p;

                const g_max = param['gMax'];
                const tau = param['tau'];
                const E_syn = param['Esyn'];

                let t = 0; // Represents the time passed since the beginning.
                
                while (t < 5 * tau) { // Rule of the thumb 5*tau, thank you Marcos Rios, and the CEFET, and the technician's course.
                    let g = g_max * t * Math.exp(1 - t/tau) / tau;
                    let a = g * (neuron.x - E_syn); // Difference to generate the synapse
                    yield a;
                    t++;
                }
            }

            s.n.activeSynapses.push(syn());
        });
    }

    /**
     * Represents an external stimulus (such as a sensory cell) to this neurons
     */
    stimulate() {
        function* syn() {
            const neuron = this;
            const param = {gMax : 0.17, tau : 1000, Esyn : 1}; // TODO: Simplify

            const g_max = param['gMax'];
            const tau = param['tau'];
            const E_syn = param['Esyn'];

            let t = 0; // Represents the time passed since the beginning.
            
            while (t < 5 * tau) { // Rule of the thumb 5*tau, thank you Marcos Rios, and the CEFET, and the technician's course.
                let g = g_max * t * Math.exp(1 - t/tau) / tau;
                //console.log(g);

                //console.log(t);
                let a = g * (neuron.x - E_syn); // Difference to generate the synapse
                //console.log(a);
                yield a;
                t++;
            }

            // console.log("ready to go!");
        }

        this.activeSynapses.push(syn());
    }

    iterUntil (until) {
        for (let j = 0.0; j < until; j += 1) {
            this.doIteration()
        }
    }
}

export { Neuron }
