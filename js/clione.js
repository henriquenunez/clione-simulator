// 23/05/2022
//import { Material, THREE } from "./three.js";

import { Neuron } from "./hr_neuron.js";
import { Statocyst } from "./statocyst_2d.js";

function makeTriangle() {
    // TODO: remove this one from here
    
    var vertices = new Float32Array( [
           0,    1, 0,
        -1/2, -1/2, 0,
         1/2, -1/2, 0,
    ]);
    
    const material = new THREE.MeshBasicMaterial({ color: new THREE.Color(93/255, 63/255, 211/255) });
    
    var geom = new THREE.BufferGeometry();
    geom.addAttribute( 'position', new THREE.BufferAttribute( vertices, 3 ) );
    var mesh = new THREE.Mesh(geom, material);

    return mesh;
}

var damp = 0.97

class Clione {
    constructor (meshGroup, boundaries = { upperleft : vec2(-6, -6), bottomRight : vec2(6, 6) }) {
        // console.log('Clione: constructor');

        this.pos = vec2();
        this.ori = 0;
        this.tail = 0;
        this.lWing = 0;
        this.rWing = 0;
        this.timer = 0;

        this.i = 0;
        this.is = [];
        this.orientations = [];
        
        // Visualization things
 
        this.body = makeTriangle();

        meshGroup.add(this.body);

        this.meshGroup = meshGroup;
        this.boundaries = boundaries;

        // Motor - Neurons
        this.tailNeuron = new Neuron("Tail");
        this.lWingNeuron = new Neuron("LW");
        this.rWingNeuron = new Neuron("RW");

        // Statocyst and its connections
        this.statocyst = new Statocyst();
        this.statocyst.neuronArray[0].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 0
        this.statocyst.neuronArray[1].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 30
        this.statocyst.neuronArray[2].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 60
        this.statocyst.neuronArray[3].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 90
        this.statocyst.neuronArray[4].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 120
        this.statocyst.neuronArray[5].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 150
        this.statocyst.neuronArray[6].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 180
        this.statocyst.neuronArray[7].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 210
        this.statocyst.neuronArray[8].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 240
        this.statocyst.neuronArray[9].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 270
        this.statocyst.neuronArray[10].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 300
        this.statocyst.neuronArray[11].addSynapse(this.tail, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 330

        this.statocyst.neuronArray[1].addSynapse(this.rWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 30
        this.statocyst.neuronArray[2].addSynapse(this.rWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 60
        this.statocyst.neuronArray[3].addSynapse(this.rWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 90
        this.statocyst.neuronArray[4].addSynapse(this.rWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 120
        this.statocyst.neuronArray[5].addSynapse(this.rWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 150
        this.statocyst.neuronArray[6].addSynapse(this.rWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}); // Angle 180

        this.statocyst.neuronArray[7].addSynapse(this.lWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 210
        this.statocyst.neuronArray[8].addSynapse(this.lWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 240
        this.statocyst.neuronArray[9].addSynapse(this.lWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 270
        this.statocyst.neuronArray[10].addSynapse(this.lWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 300
        this.statocyst.neuronArray[11].addSynapse(this.lWingNeuron, {gMax : 0.2, tau : 1000, Esyn : 1}) ; // Angle 330
        
        // Tail excites wings // 'gMax', 'tau' and 'Esyn'
        //this.tailNeuron.addSynapse(this.lWingNeuron, {gMax : 0.50, tau : 1000, Esyn : 1});
        //this.tailNeuron.addSynapse(this.rWingNeuron, {gMax : 0.50, tau : 1000, Esyn : 1});

        // Wings mutually inhibit
        //this.rWingNeuron.addSynapse(this.lWingNeuron, {gMax : 0.17, tau : 1000, Esyn : -1.5})
        //this.lWingNeuron.addSynapse(this.rWingNeuron, {gMax : 0.17, tau : 1000, Esyn : -1.5})

        // Wings excitate tail
        //this.rWingNeuron.addSynapse(this.tailNeuron, {gMax : 0.3, tau : 1000, Esyn : 2})
        //this.lWingNeuron.addSynapse(this.tailNeuron, {gMax : 0.3, tau : 1000, Esyn : 2})
    }

    withinBoundaries() {

        if (this.pos.x < this.boundaries.upperleft.x || this.pos.x > this.boundaries.bottomRight.x ||
            this.pos.y < this.boundaries.upperleft.y || this.pos.y > this.boundaries.bottomRight.y) {
                return false;
        }

        return true
    }

    /** Movement
     * How to implement the movement? That was a question I had to look for.
     * Differential equations could be used, but I was short in time. Instead,
     * I decided to store the acceleration in each of the variables, and in each iteration the
     * damp factor will multiply the variable.
     */
    movement() {
        console.log('Clione: moving ' + JSON.stringify(this.pos));

        // Check boundaries
        if (!this.withinBoundaries()) {
            this.ori = this.ori + Math.PI;
        }

        if (this.ori > 360) {
            this.ori -= 360;
        }

        // Actual movement
        let displac = pol2rec(this.tail, this.ori);
        let displacLWing = pol2rec(this.tail, this.ori - Math.PI / 2);
        let displacRWing = pol2rec(this.tail, this.ori + Math.PI / 2);

        this.pos = vec2Add(vec2Add(vec2Add(this.pos, displac), displacLWing), displacRWing);
        this.ori = this.ori - this.lWing + this.rWing;

        //console.log(this.ori);
        //this.body.position.set(this.pos.x, this.pos.y, 0);

        // Dampening
        this.tail *= damp;
        this.lWing *= damp;
        this.rWing *= damp;

        // Updating statocyst
        this.statocyst.setOrientation(this.ori);
    }

    // Evaluate the signals coming from the statocyst
    // and check which neuron should be activated.
    mock_motorGroups() {
        if (Math.random() < 0.05)
            this.tail += 0.02;

        if (Math.random() < 0.05)
            this.lWing += 0.01;

        if (Math.random() < 0.05)
            this.rWing += 0.01;
    }

    // Evaluate the signals coming from the statocyst
    // and check which neuron should be activated.
    motorGroups() {
        if (this.tailNeuron.x > 0.5)
            this.tail += 0.02;

        if (this.lWingNeuron.x > 0.5)
            this.lWing += 0.02; //Math.random() * 0.02;

        if (this.rWingNeuron.x > 0.5)
            this.rWing += 0.02; //Math.random() * 0.02;
    }

    // Not necessarily a render call will be one animation
    // It is actually more likely that
    doIteration () {
        // this.statocyst.doIteration(); // 1000 times or so
        this.tailNeuron.interact();
        this.lWingNeuron.interact();
        this.rWingNeuron.interact();

        // Just for testing
        this.tailNeuron.iterUntil(10);
        this.lWingNeuron.iterUntil(10);
        this.rWingNeuron.iterUntil(10);

        //console.log(this.tailNeuron.x)
        //console.log(this.lWingNeuron.x)
        //console.log(this.rWingNeuron.x)

        this.motorGroups();
        // this.mock_motorGroups();
        this.movement();

        // Register orientations
        if (this.i < 10000000) {
            this.orientations.push(this.ori);
            this.is.push(this.i);
            this.i += 1;
        }

        // this.statocyst.orientation = this.posOrientation.r;
    }
    
    iterUntil (until) {
        for (let j = 0.0; j < until; j += 1) {
            this.doIteration();
        }
    }

    // We can pull the data directly from the neurons
}

export { Clione }
