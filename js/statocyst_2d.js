/**
 * statocyst_2d.js
 * simulates a statocyst found in Clione
 */

import { Neuron } from "./hr_neuron.js";

// 2D version of the statocyst

const sensoryNeuronNumber = 12

class Statocyst {
    
    constructor () {
        
        this.neuronArray = [];
        this.angle = 0; // wrt up direction vector

        for (let index = 0; index < 12; index++) {
            this.neuronArray.push(new Neuron())            
        }

        for (let i = 0; i < 12; i++) {
            for (let j = 0; j < 12; j++) {
                if (i != j) {
                    // Add inhibitory connections from a neuron to all the others
                    this.neuronArray[i].addSynapse(this.neuronArray[j], {gMax : 0.5, tau : 1000, Esyn : -1.5});
                }
            } 
        }
    }

    /**
     * Updates the orientation and recomputes the
     * generated signals.
     * @param {number} angle in degrees
     */
    setOrientation (angle) {
        this.angle = angle;

        // Since the 0 deg orientation should lie in the 
        // Easy computation
        let base_idx = Math.round(Math.abs(this.angle) / 30) % 12; // 360 / 12
        this.neuronArray[base_idx].stimulate();
    }

    /**
     * Performs iteration
     */
    doIteration () {
        // Compute for every neuron
        // TODO do observationthis.neuronArray.forEach((n) => {n.doIteration()})
        this.neuronArray.forEach((n) => {n.doIteration()})
    }
}

export { Statocyst } //TM WM (tail and wing motor)
