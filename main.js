//import { importFile, posStringToPos } from "./js/importer.js";
//import { colorMap } from "./js/colormap.js";
import { Neuron } from "./js/hr_neuron.js";
import { Clione } from "./js/clione.js";

const renderer = new THREE.WebGLRenderer();
const renderContainer = document.querySelector("#c");
renderContainer.appendChild(renderer.domElement);

const fov = 75;
const aspect = 2;  // the canvas default
const near = 0.1;
const far = 1000;
const camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
camera.position.z = 10;

// THREE.js bootstrap
const scene = new THREE.Scene();
const meshGroup = new THREE.Group();
let clione = new Clione(meshGroup);
scene.add(meshGroup);

function updateCamera () {
    const w = renderContainer.clientWidth;
    const h = renderContainer.clientHeight;

    console.log('Renderer w: ' + w.toString() + ' Renderer h: ' + h.toString());

    camera.aspect = w / h;
    camera.updateProjectionMatrix();
    renderer.setSize(w, h);
}

//100000
console.log("Interacting...");
//clione.iterUntil(100000);

//await new Promise(r => setTimeout(r, 60000))

// Starting neurons
// var nA = new Neuron();
// var nB = new Neuron();
// let neurons = [ nA, nB ];

// for (let i = 0; i < 1000000 * 2; i++) {
//     neurons.forEach(n => {
//         n.interact();
//     });

//     neurons.forEach(n => {
//         n.doIteration();
//     });
// }

// Render
function render(time) {
    clione.doIteration(); // Updates scene.
    //clione.iterUntil(10); // Updates scene.

    //time *= 0.001;

    meshGroup.position.x = clione.pos.x;
    meshGroup.position.y = clione.pos.y;
    meshGroup.rotation.z = clione.ori - Math.PI / 2;

    renderer.render(scene, camera);
    requestAnimationFrame(render);
}

// Get current screen parameters
updateCamera();
requestAnimationFrame(render);

// UI INTERACTIONS
var count = 0;
function onWindowResize() {
    console.log('resized: ' + count);
    count++;

    updateCamera();
}

window.onresize = _.debounce(onWindowResize, 100);
//await new Promise(r => setTimeout(r, 60000));
console.log("Done!");

// Subsampling
function subsample(el, idx, arr) {
    if (idx % 1000 == 0) {
        return true;
    }
    return false;
}

//;


console.log('A');
console.log(clione.lWingNeuron.x_s.length);

var tail = clione.tailNeuron.x_s.filter(subsample);
var left_wing = clione.lWingNeuron.x_s.filter(subsample);
var right_wing = clione.rWingNeuron.x_s.filter(subsample);
var data_labels = clione.tailNeuron.t_s.filter(subsample).map(Math.round);

// Plot how the animal is behaving
const ctx = document.getElementById('myChart').getContext('2d');
const myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels : data_labels,
        datasets: [
        {
            label: 'Tail',
            data: tail,
            fill: false,
            borderColor: 'rgb(240, 130, 130)',
            tension: 0.1
          },
          {
            label: 'Left Wing',
            data: left_wing,
            fill: false,
            borderColor: 'rgb(93, 63, 211)',
            tension: 0.1
          },
          {
            label: 'Right Wing',
            data: right_wing,
            fill: false,
            borderColor: 'rgb(93, 255, 150)',
            tension: 0.1
          }]
    },
    // options: {
    //     scales: {
    //         y: {
    //             beginAtZero: true
    //         }
    //     }
    // }
});

function subsample_2(el, idx, arr) {
    if (idx % 100 == 0) {
        return true;
    }
    return false;
}

const cl_orientation = clione.orientations.filter(subsample_2);
const cl_is = clione.is.filter(subsample_2);

const ctx_2 = document.getElementById('chart_2').getContext('2d');
const chart_2 = new Chart(ctx_2, {
    type: 'line',
    data: {
        labels : cl_is,
        datasets: [
        {
            label: 'Orientation',
            data: cl_orientation,
            fill: false,
            borderColor: 'rgb(240, 130, 130)',
            tension: 0.1
          }]
    },
    // options: {
    //     scales: {
    //         y: {
    //             beginAtZero: true
    //         }
    //     }
    // }
});
console.log('B');
